<?php

include("emagid.db.php");
include("emagid.consts.php");

$site_routes = [
    'routes' => [
        [
            'name' => 'login',
            'area' => 'admin',
            'template' => 'admin',
            'pattern' => "admin/login/{?action}",
            'controller' => 'login',
            'action' => 'login',
        ],
        [
            'name' => 'email',
            'area' => 'admin',
            'template' => 'admin',
            'pattern' => "admin/emails",
            'controller' => 'email_list',
            'action' => 'redir',
            'authorize' => [
                'roles' => ['admin'],
                'login_url' => '/admin/login'
            ]
        ],
        [
            'name' => 'admin',
            'area' => 'admin',
            'template' => 'admin',
            'pattern' => "admin/{?controller}/{?action}/{?id}",
            'controller' => 'dashboard',
            'action' => 'index',
            'authorize' => [
                'roles' => ['admin'],
                'login_url' => '/admin/login'
            ]
        ],
        [
            'name' => 'page',
            'pattern' => "page/{page_slug}",
            'controller' => 'pages',
            'action' => 'page'
        ],
        [
            'name' => 'accountSetup',
            'pattern' => "account/activate/{hash}",
            'controller' => 'account',
            'action' => 'activate'
        ],
        [
            'name' => 'products',
            'pattern' => "products/{slug}",
            'controller' => 'products',
            'action' => 'index'
        ],
        [
            'name' => 'status',
            'pattern' => "status/{slug}",
            'controller' => 'status',
            'action' => 'index'
        ],
        [
            'name' => 'service',
            'pattern' => "service/{service_slug}",
            'controller' => 'services',
            'action' => 'service'
        ],
        [
            'name' => 'confirm',
            'pattern' => "browse/confirm",
            'controller' => 'browse',
            'action' => 'confirm'
        ],
        [
            'name' => 'weddingbands',
            'pattern' => "browse/weddingbands",
            'controller' => 'browse',
            'action' => 'weddingband'
        ],
        [
            'name' => 'rings',
            'pattern' => "browse/rings",
            'controller' => 'browse',
            'action' => 'ring'
        ],
        [
            'name' => 'diamonds',
            'pattern' => "browse/diamonds",
            'controller' => 'browse',
            'action' => 'diamond'
        ],
        [
            'name' => 'category',
            'pattern' => "category/{category_slug}",
            'controller' => 'category',
            'action' => 'index'
        ],
        [
            'name' => 'resetPassword',
            'pattern' => "reset/password/{hash}",
            'controller' => 'reset',
            'action' => 'password'
        ]
]
];

$emagid_config = array(
    'debug' => true,          // When debug is enabled, Kint debugging plugin is enabled.
    //  you can use d($my_var) , dd($my_var) , Kint::Trace() , etc...
    //  documentation available here : http://raveren.github.io/kint/

    'root' => SITE_URL,
    'template' => 'emagidCheckin',  // template must be found in /temlpates/<template_name>/<template_name>.php
    // so in this example we will have /templates/default/default.php
    // please open the template file to see how the view is being rendered .

    'connection_string' => array(
        'driver' => DB_DRIVER,
        'db_name' => DB_NAME,
        'username' => DB_USER,
        'password' => DB_PWD,
        'host' => DB_HOST
    ),

    'include_paths' => array(
        'libs/Mandrill'
    ),

    'email' => array(
//        'api_key' => 'fwV0KfGfdAOKPtaxYWhcBw',
        'api_key' => 'pGd74perhIj4uZ4kS5wWTg',
        'from' => array(
            'email' => 'info@bbydiamonds.com',
            'name' => 'BBY Diamonds'
        )
    ),
); 
