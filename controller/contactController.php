<?php

class contactController extends siteController
{

    public function index(Array $params = [])
    {

        $this->configs['Meta Title'] = "Contact | nielsen sports";
        $this->loadView($this->viewData);
    }

    public function index_post()
    {
        $response = ['status'=>false,
                     'msg'=>'failed to save donation'];
        $obj = \Model\Contact::loadFromPost();
        if($obj->save()){
//          $n = new \Notification\MessageHandler('We will contact you shortly.');
//          $_SESSION["notification"] = serialize($n);
            $imgs = [];
            $gif = [];
            $email_image = null;
            if(isset($_POST['image'])){
                $img = $_POST['image'];
                $image = \Model\Snapshot_Contact::getItem($img);
                $image->contact_id = $obj->id;
                if($image->save()){
                    $response['image']=$image;
                    $email_image = $image->get_image_url();
                }
            }
            if(isset($_POST['gif'])){
                $_gif = $_POST['gif'];
                $gif = \Model\Gif::getItem($_gif);
                $gif->contact_id = $obj->id;
                if($gif->save()){
                    $response['gif'] = $gif;
                    $email_image = $gif->get_image_url();
                }
            }

            if($email_image == null)
                $email_image = 'Thanks for your donation!';
            else
                $email_image = "<img src='$email_image'>";

            $email = new Email\MailMaster();
            $email->setTo(['email' => $obj->email, 'name' => $obj->name,  'type' => 'to'])->setMergeTags(['CONTENT'=>$email_image])->setTemplate('allied-donation')->send();

            $response['status'] = true;
            $response['msg'] = 'Success';
            $response['contact'] = $obj;
        }

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function make_gif_post(){
        $response = ['status'=>true];
        if(isset($_POST['images'])){
            $imgs = [];
            foreach($_POST['images'] as $img){

                $image = $this->save_img($img);
                $imgs[] = $image;
            }
            $response['images'] = $imgs;

            header('Content-Type: application/json');
            echo json_encode($response);
        }
    }

    public function make_frame_post(){
        $response = ['status'=>true];
        header('Content-Type: application/json');
        if(isset($_POST['image'])){
            $image = $this->save_img($_POST['image']);
            $response['image'] = $image;

        } else {
            $response['status'] = false;
        }
        echo json_encode($response);
    }

    public function save_img_post(){
        $response = ['status'=>true];
        header('Content-Type: application/json');

        if(isset($_POST['gif'])){
            $gif = new \Model\Gif();
            $gif->frames = json_encode($_POST['images']);
            foreach($_POST['images'] as $img){
//                $_img = \Model\Snapshot_Contact::getItem($img);
                \Model\Snapshot_Contact::delete($img);
            }
            if (!is_dir(UPLOAD_PATH.'Gifs'.DS)){
                mkdir(UPLOAD_PATH.'Gifs'.DS, 0777, true);
            }


            $url = $_POST['gif'];
            $fileName = uniqid().'.gif';
            $img = UPLOAD_PATH.'Gifs'.DS.$fileName;


//                $gif->image = $fileName;
            $gif->image = $url;
            $gif->contact_id = 0;
            $gif->frames = json_encode($_POST['images']);
            if($gif->save()){
                $response['gif'] = $gif;
            } else {
                $response['status'] = false;
                $response['errors'] = $gif->errors;
                $response['msg'] = "Gif Save Failed";
            }
        } else if(isset($_POST['image'])){
            $image = $this->save_img($_POST['image']);
            $response['image']=$image;
        }
        echo json_encode($response);
    }

    public function save_img($_img, $obj = null){
        $img = $_img;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $fileName = uniqid().'.png';
        $file = UPLOAD_PATH.'Snapshots'.DS.$fileName;

        if (!is_dir(UPLOAD_PATH.'Snapshots'.DS)){
            mkdir(UPLOAD_PATH.'Snapshots'.DS, 0777, true);
        }

        $success = file_put_contents($file, $data);

        $image = new \Model\Snapshot_Contact();
        if($obj) $image->contact_id = $obj->id;
        else $image->contact_id = 0;
        $image->image = $fileName;
        $image->save();

        if($obj){
            $obj->save();
        }
        return($image);
    }
}